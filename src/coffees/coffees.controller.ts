import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Inject,
  Param,
  Patch,
  Post,
  Query,
  Res,
} from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { ApiForbiddenResponse, ApiResponse, ApiTags } from '@nestjs/swagger';
import { response } from 'express';
import { request } from 'http';
import { PaginationQueryDto } from './../common/dto/pagination-query.dto';
import { ParseIntPipe } from './../common/pipes/parse-int.pipe';
import { Protocol } from './../decorators/protocol.decorator';
import { CoffeesService } from './coffees.service';
import { CreateCoffeeDto } from './dto/create-coffee.dto';
import { UpdateCoffeeDto } from './dto/update-coffee.dto';
@ApiTags('coffees')
@Controller('coffees')
export class CoffeesController {
  constructor(private readonly coffeServices: CoffeesService) {
    console.log('CREATEDs');
  }
  @ApiForbiddenResponse({ description: 'Forbidden' })
  @Get()
  async findAll(
    @Protocol('https') protocol: string,
    @Query() paginationQuery: PaginationQueryDto,
  ) {
    console.log(protocol);

    /* const { limit, offset } = paginationQuery;
    return `el limit es ${limit} offset: ${offset}`;*/
    //await new Promise((resolve) => setTimeout(resolve, 5000));
    return this.coffeServices.findAll(paginationQuery);
  }
  @Get(':id')
  findOne(@Param('id', ParseIntPipe) id: number) {
    console.log(id);
    const coffe = this.coffeServices.findOne(id);
    if (!coffe) {
      throw new HttpException(
        `El coffe con el id ${id} no se ha encontrado`,
        HttpStatus.NOT_FOUND,
      );
    }
    return coffe;
  }
  @Post()
  create(@Body() CreateCoffeeDto: CreateCoffeeDto) {
    return this.coffeServices.create(CreateCoffeeDto);
  }
  @Patch(':id')
  update(@Param('id') id, @Body() UpdateCoffeeDto: UpdateCoffeeDto) {
    return this.coffeServices.update(id, UpdateCoffeeDto);
  }
  @Delete(':id')
  remove(@Param('id') id) {
    return this.coffeServices.remove(id);
  }
}
