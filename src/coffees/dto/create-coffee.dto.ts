import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
export class CreateCoffeeDto {
  @ApiProperty({ description: 'El nombre del cafe' })
  @IsString()
  readonly name: string;
  @ApiProperty({ description: 'La marca del cafe' })
  @IsString()
  readonly brand: string;
  @ApiProperty({ example: [] })
  @IsString({ each: true })
  readonly flavors: string[];
}
