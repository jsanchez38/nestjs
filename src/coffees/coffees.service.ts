import { Inject, Injectable, NotFoundException, Scope } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PaginationQueryDto } from './../common/dto/pagination-query.dto';
import { EventEntity } from '../events/entities/event.entity';
import { Connection, Repository } from 'typeorm';
import { COFFEE_BRANDS } from './coffess.constants';
import { CreateCoffeeDto } from './dto/create-coffee.dto';
import { UpdateCoffeeDto } from './dto/update-coffee.dto';
import { Coffee } from './entities/coffee.entity';
import { Flavor } from './entities/flavor.entity';

@Injectable({ scope: Scope.REQUEST })
export class CoffeesService {
  constructor(
    @InjectRepository(Coffee)
    private readonly coffeRepository: Repository<Coffee>,
    @InjectRepository(Flavor)
    private readonly FlavorRepository: Repository<Flavor>,
    private readonly connection: Connection,
    @Inject(COFFEE_BRANDS) CoffeBrands: string[],
    @Inject(COFFEE_BRANDS) CoffeBrands2: string[],
  ) {
    console.log(CoffeBrands);
    console.log(CoffeBrands2);
    console.log('se genera cada vez por peticion');
  }
  findAll(paginationQuery: PaginationQueryDto) {
    const { limit, offset } = paginationQuery;
    return this.coffeRepository.find({
      relations: ['flavors'],
      skip: offset,
      take: limit,
    });
  }
  async findOne(id: any) {
    console.log(id);

    const coffe = await this.coffeRepository.findOne({
      where: { id },
      relations: ['flavors'],
    });
    if (!coffe) {
      throw new NotFoundException(`Coffee #${id} not found`);
    }

    return coffe;
  }
  async create(createCoffe: CreateCoffeeDto) {
    const flavors = await Promise.all(
      createCoffe.flavors.map((name) => this.preloadFlavorByName(name)),
    );
    const coffe = this.coffeRepository.create({ ...createCoffe, flavors });
    return this.coffeRepository.save(coffe);
  }
  async update(id: string, updateCoffe: UpdateCoffeeDto) {
    const flavors =
      updateCoffe.flavors &&
      (await Promise.all(
        updateCoffe.flavors.map((name) => this.preloadFlavorByName(name)),
      ));

    const coffe = await this.coffeRepository.preload({
      id: +id,
      ...updateCoffe,
      flavors,
    });
    if (!coffe) {
      throw new NotFoundException('no se ha encontrado');
    }

    return this.coffeRepository.save(coffe);
  }
  async remove(id: string) {
    const coffeDelete = await this.findOne(id);
    return this.coffeRepository.remove(coffeDelete);
  }

  // private async preloadFlavorByName(name: string): Promise<Flavor> {
  //   const existFlavor = await this.FlavorRepository.find({
  //     where: { name },
  //   });

  //   if (existFlavor.length > 0) {
  //     return existFlavor;
  //   }
  //   return this.FlavorRepository.create({ name });
  // }
  async recomendedCoffe(coffe: Coffee) {
    const queryrunner = this.connection.createQueryRunner();

    await queryrunner.connect();
    await queryrunner.startTransaction();
    try {
      coffe.recommendation++;
      const recomendedEvent = new EventEntity();
      recomendedEvent.name = 'recommend_coffee';
      recomendedEvent.type = 'coffe';
      recomendedEvent.payload = { coffeId: coffe.id };
      await queryrunner.manager.save(coffe);
      await queryrunner.manager.save(recomendedEvent);
      await queryrunner.commitTransaction();
    } catch (err) {
      await queryrunner.rollbackTransaction();
    } finally {
      await queryrunner.release();
    }
  }

  private async preloadFlavorByName(name: string): Promise<Flavor> {
    const existingFlavor = await this.FlavorRepository.findOne({
      where: {
        name: name,
      },
    });
    if (existingFlavor) {
      return existingFlavor;
    }
    return this.FlavorRepository.create({ name });
  }
}
