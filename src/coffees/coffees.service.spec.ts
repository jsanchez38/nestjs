import { NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { LoggingMiddleware } from 'src/common/middleware/loggin.middleware';
import { EventEntity } from 'src/events/entities/event.entity';
import { Connection, Repository } from 'typeorm';
import { CoffeesService } from './coffees.service';
import { COFFEE_BRANDS, COFFEE_BRANDS2 } from './coffess.constants';
import { Coffee } from './entities/coffee.entity';
import { Flavor } from './entities/flavor.entity';

type MockRepository<T = any> = Partial<Record<keyof Repository<T>, jest.Mock>>;
const createMockRepository = <T = any>(): MockRepository<T> => ({
  findOne: jest.fn(),
  create: jest.fn(),
});

describe('CoffeesService', () => {
  let service: CoffeesService;
  let coffeRepository: MockRepository;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CoffeesService,
        { provide: EventEntity, useValue: {} },
        { provide: Connection, useValue: {} },

        { provide: LoggingMiddleware, useValue: {} },

        { provide: COFFEE_BRANDS, useValue: {} },
        { provide: COFFEE_BRANDS2, useValue: {} },
        {
          provide: getRepositoryToken(Flavor),
          useValue: createMockRepository(),
        },
        {
          provide: getRepositoryToken(Coffee),
          useValue: createMockRepository(),
        },
      ],
    }).compile();

    service = await module.resolve<CoffeesService>(CoffeesService);
    coffeRepository = module.get<MockRepository>(getRepositoryToken(Coffee));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findOne', () => {
    describe('Si existe el id', () => {
      it('debe devolver el objeto', async () => {
        const coffeID = 1;
        const expectedCoffe = {};
        coffeRepository.findOne.mockReturnValue(expectedCoffe);
        const coffe = await service.findOne(coffeID);
        expect(coffe).toEqual(expectedCoffe);
      });
    });
    describe('No existe el id', () => {
      it('debe devolver un notfound', async () => {
        const coffeeId = '1';

        coffeRepository.findOne.mockReturnValue(undefined);

        try {
          await service.findOne(coffeeId);
        } catch (err) {
          expect(err).toBeInstanceOf(NotFoundException);
          expect(err.message).toEqual(`Coffee #${coffeeId} not found`);
        }
      });
    });
  });
});
