import {
  Injectable,
  MiddlewareConsumer,
  Module,
  NestModule,
  Scope,
} from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LoggingMiddleware } from 'src/common/middleware/loggin.middleware';
import { EventEntity } from '../events/entities/event.entity';
import { CoffeesController } from './coffees.controller';
import { CoffeesService } from './coffees.service';
import { COFFEE_BRANDS, COFFEE_BRANDS2 } from './coffess.constants';
import { Coffee } from './entities/coffee.entity';
import { Flavor } from './entities/flavor.entity';
class ConfigService {}
class DevelopmentCondig {}
class ProductionServices {}
@Injectable()
export class CoffeeBrandFactory {
  create() {
    return ['test1', 'test2'];
  }
}
@Module({
  imports: [
    TypeOrmModule.forFeature([Coffee, Flavor, EventEntity]),
    ConfigModule,
  ],
  controllers: [CoffeesController],
  providers: [
    CoffeesService,
    CoffeeBrandFactory,
    {
      provide: ConfigService,
      useClass:
        process.env.NODE_ENV === 'development'
          ? DevelopmentCondig
          : ProductionServices,
    },
    {
      provide: COFFEE_BRANDS,
      useValue: ['test1', 'test2'],
    },
    {
      provide: COFFEE_BRANDS,
      useValue: ['test1', 'test2'],
    },
    {
      provide: COFFEE_BRANDS2,
      useFactory: (brandsFactory: CoffeeBrandFactory) => {
        brandsFactory.create();
      },
      scope: Scope.TRANSIENT,
      inject: [CoffeeBrandFactory],
    },
  ],
  exports: [CoffeesService],
})
// export class CoffeesModule implements NestModule {
//   configure(consumer: MiddlewareConsumer) {
//     consumer.apply(LoggingMiddleware).forRoutes('*');
//   }
// }
export class CoffeesModule {}
