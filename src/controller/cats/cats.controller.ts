import {
  Controller,
  Get,
  Post,
  Body,
  HttpException,
  HttpStatus,
  UseFilters,
  Param,
  ParseIntPipe,
  Delete,
} from '@nestjs/common';
import { CreateCatDto } from './dto/create-cat.dto';
import { CatsService } from './cats.service';
import { Cat } from './interfaces/cat.interface';
import { ForbiddenException } from './exception/forbidden.exception';
import { AllExceptionsFilter } from './filter/all-exceptions.filter';
import { Persona } from './entity/persona.entity';
import { CreateHijos } from './dto/create-hijo.dto';
import { Hijos } from './entity/hijos.entity';
@Controller('cats')
export class CatsController {
  constructor(private catsService: CatsService) {}

  @Get()
  async findAll(): Promise<Persona[]> {
    return await this.catsService.findAll();
    //customizar error
    // throw new ForbiddenException();
  }
  @Post()
  create(@Body() createUserDto: CreateCatDto): Promise<Persona> {
    return this.catsService.create(createUserDto);
  }

  @Get(':id')
  findOne(@Param('id') id: number): Promise<Persona[]> {
    return this.catsService.findOne(id);
  }

  @Delete(':id')
  remove(@Param('id') id: string): Promise<void> {
    return this.catsService.remove(id);
  }
}
