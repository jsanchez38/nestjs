import { HttpException, HttpStatus } from '@nestjs/common';

export class ForbiddenException extends HttpException {
  constructor() {
    super('No se ha encontrado', HttpStatus.FORBIDDEN);
  }
}
