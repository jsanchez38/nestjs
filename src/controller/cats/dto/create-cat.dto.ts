import { Hijos } from '../entity/hijos.entity';

export class CreateCatDto {
  id: string;
  nombre: string;
  Apellido: string;
  Hijos: Hijos;
}
