import { Persona } from '../entity/persona.entity';

export class CreateHijos {
  id: string;
  nombre: string;
  apellido: string;
  persona: Persona;
}
