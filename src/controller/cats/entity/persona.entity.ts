import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { Hijos } from './hijos.entity';

@Entity({ name: 'Persona' })
export class Persona {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nombre: string;

  @Column()
  Apellido: string;

  @OneToMany(() => Hijos, (Hijo) => Hijo.persona, { cascade: ['insert'] })
  Hijos: Hijos[];
}
