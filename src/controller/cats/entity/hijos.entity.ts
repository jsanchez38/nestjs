import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { Persona } from './persona.entity';
@Entity({ name: 'Hijos' })
export class Hijos {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nombre: string;

  @Column()
  apellido: string;

  @ManyToOne((type) => Persona, (persona) => persona.Hijos)
  @JoinColumn({ name: 'Persona_id' })
  persona: Persona;
}
