import { Module } from '@nestjs/common';
import { CatsController } from './cats.controller';
import { CatsService } from './cats.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Persona } from './entity/persona.entity';
import { Hijos } from './entity/hijos.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Persona, Hijos])],

  controllers: [CatsController],
  providers: [CatsService],
  exports: [CatsService],
})
export class CatsModule {}
