import { Injectable } from '@nestjs/common';
import { Cat } from './interfaces/cat.interface';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Persona } from './entity/persona.entity';
import { CreateCatDto } from './dto/create-cat.dto';
import { Hijos } from './entity/hijos.entity';
import { Console } from 'console';
@Injectable()
export class CatsService {
  constructor(
    @InjectRepository(Persona)
    private usersRepository: Repository<Persona>,
  ) {}

  create(createUserDto: CreateCatDto): Promise<Persona> {
    const user = new Persona();
    user.nombre = createUserDto.nombre;
    user.Apellido = createUserDto.Apellido;
    user.Hijos = [];
    const obj = { ...createUserDto.Hijos };
    console.log(obj);
    console.log('--->', createUserDto.Hijos);

    console.log(user);
    //console.log(createUserDto.Hijos.lenght);
    // const hijo = new Hijos();
    // hijo.nombre = createUserDto.Hijos.nombre;
    // hijo.Apellido = createUserDto.Hijos.Apellido;
    // console.log(user.Hijos);

    // user.Hijos.push(hijo);
    console.log(user.Hijos);
    return this.usersRepository.save(user);
  }

  async findAll(): Promise<Persona[]> {
    const x = this.usersRepository.find({
      relations: {
        Hijos: true,
      },
    });
    console.log(x);
    return x;
  }

  findOne(id: any): Promise<Persona[]> {
    const total = id;
    console.log('---->', total.id);
    return this.usersRepository.find({ where: { id }, relations: ['Hijos'] });
  }

  async remove(id: string): Promise<void> {
    await this.usersRepository.delete(id);
  }
}
