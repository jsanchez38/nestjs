import { HttpStatus, INestApplication, ValidationPipe } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CoffeesModule } from '../../src/coffees/coffees.module';
import { CreateCoffeeDto } from '../../src/coffees/dto/create-coffee.dto';
import * as request from 'supertest';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';

describe('[Feature] Coffees - /coffees', () => {
  const coffee = {
    name: 'Shipwreck Roast',
    brand: 'Buddy Brew',
    flavors: ['chocolate', 'vanilla'],
  };
  const coffeeUpdate = {
    name: 'Shipwreck Roast2',
    brand: 'Buddy Brew 2',
    flavors: ['chocolate', 'vanilla'],
  };
  let app: NestFastifyApplication;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        CoffeesModule,
        TypeOrmModule.forRoot({
          type: 'postgres',
          host: 'localhost',
          port: 5433,
          username: 'postgres',
          password: 'pass123',
          database: 'postgres',
          autoLoadEntities: true,
          synchronize: true,
        }),
      ],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(
      new ValidationPipe({
        whitelist: true,
        transform: true,
        forbidNonWhitelisted: true,
        transformOptions: {
          enableImplicitConversion: true,
        },
      }),
    );

    app = moduleFixture.createNestApplication();
    app = moduleFixture.createNestApplication<NestFastifyApplication>(
      new FastifyAdapter(),
    );

    await app.init();
    await app.getHttpAdapter().getInstance().ready();
  });

  it('Create [POST /]', () => {
    return request(app.getHttpServer())
      .post('/coffees')
      .send(coffee as CreateCoffeeDto)
      .expect(HttpStatus.CREATED);
  });
  it(`Create [GET /] Fastify`, () => {
    return app
      .inject({
        method: 'POST',
        url: '/coffees',
        payload: coffee as CreateCoffeeDto,
      })
      .then((result) => {
        console.log('----->', result);
        expect(result.statusCode).toEqual(HttpStatus.CREATED);
      });
  });
  it('Create [GET ALL /]', () => {
    return request(app.getHttpServer()).get('/coffees').expect(HttpStatus.OK);
  });

  it(`Create [GET /] Fastify`, () => {
    return app
      .inject({
        method: 'GET',
        url: '/coffees',
      })
      .then((result) => {
        console.log('----->', result);
        expect(result.statusCode).toEqual(HttpStatus.OK);
      });
  });
  it('Create [GET ID /]', () => {
    return request(app.getHttpServer()).get('/coffees/2').expect(HttpStatus.OK);
  });
  it(`Create [GET /] Fastify`, () => {
    return app
      .inject({
        method: 'GET',
        url: '/coffees/2',
      })
      .then((result) => {
        console.log('----->', result);
        expect(result.statusCode).toEqual(200);
      });
  });
  it('Update one [PATCH /:id]', () => {
    return request(app.getHttpServer())
      .patch('/coffees/2')
      .send(coffeeUpdate as CreateCoffeeDto)

      .expect(HttpStatus.OK);
  });
  it(`Create [PATCH /:id] Fastify`, () => {
    return app
      .inject({
        method: 'PATCH',
        url: '/coffees/2',
        payload: coffeeUpdate as CreateCoffeeDto,
      })
      .then((result) => {
        console.log('----->', result);
        expect(result.statusCode).toEqual(200);
      });
  });
  it('Delete one [DELETE /:id]', () => {
    return request(app.getHttpServer())
      .delete('/coffees/1')
      .expect(HttpStatus.NOT_FOUND);
  });
  it(`Create [PATCH /:id] Fastify`, () => {
    return app
      .inject({
        method: 'DELETE',
        url: '/coffees/1',
      })
      .then((result) => {
        console.log('----->', result);
        expect(result.statusCode).toEqual(HttpStatus.NOT_FOUND);
      });
  });
  afterAll(async () => {
    await app.close();
  });
});
